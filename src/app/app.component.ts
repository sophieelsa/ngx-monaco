import { Component, ViewChild } from '@angular/core';
import { filter, take } from 'rxjs/operators';
import {
  MonacoEditorComponent,
  MonacoEditorConstructionOptions,
  MonacoEditorLoaderService,
  MonacoStandaloneCodeEditor,
} from '@materia-ui/ngx-monaco-editor';

@Component({
  selector: 'app-root',
  templateUrl: `./app.component.html`,
  styleUrls: [`./app.component.scss`]
})
export class AppComponent {
  @ViewChild(MonacoEditorComponent, { static: false })
  monacoComponent: MonacoEditorComponent;
  editorOptions: MonacoEditorConstructionOptions = {
    theme: 'vs-dark',
    language: 'text/html',
    minimap: {
        enabled: false
    },
    lineNumbers: 'off',
    automaticLayout: true
  };
  
  model: any;

  code = {
    javascript: [
      '"use strict";',
      'function Person(age) {',
      '	if (age) {',
      '		this.age = age;',
      '	}',
      '}',
      'Person.prototype.getAge = function () {',
      '	return this.age;',
      '};'
    ].join('\n'),
    html: '<p>Live Reloading enabled</p>',
    css: 'p {color: blue;}'
  };
  

  constructor(private monacoLoaderService: MonacoEditorLoaderService) {
    this.monacoLoaderService.isMonacoLoaded$
      .pipe(
        filter(isLoaded => isLoaded),
        take(1)
      )
      .subscribe(() => {
            var modelUri = monaco.Uri.parse("a://b/foo.json"); // a made up unique URI for our model
            // this.model = monaco.editor.createModel(jsonCode, "json", modelUri);
      });
  }

  editorInit(editor: MonacoStandaloneCodeEditor) {
  }

  setCodeByLanguage(lang: string) {
    const model = monaco.editor.getModels()[0];
    monaco.editor.setModelLanguage(model, `text/${lang}`);
    model.setValue(this.code[lang])
  }

  switchLanguage(lang: string) {
    console.log(lang)
    this.setCodeByLanguage(lang);
  }
}
